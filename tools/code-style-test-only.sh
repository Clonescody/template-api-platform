#!/usr/bin/env bash

export PROJECT=mcs-team-template-api-platform

docker run -t --rm  --entrypoint="" \
-v $BITBUCKET_CLONE_DIR/tools:/root/tools \
-v $BITBUCKET_CLONE_DIR/docker/image/final/apache-php7/src:/var/www \
--workdir=/root/tools \
${PROJECT}/web-base \
./php-cs-fixer fix /var/www/src --dry-run --diff --rules=@Symfony,@PhpCsFixer
