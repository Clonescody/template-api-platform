<?php

// src/Command/CreateUserCommand.php

namespace App\Command;

use App\Entity\Book;
use Faker\Factory;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBooksCommand extends Command
{
	protected static $defaultName = 'app:create-books';

	private $cache;
	private $faker;

	public function __construct(CacheInterface $cache)
	{
		$this->cache = $cache;
		$this->faker = Factory::create('fr_FR');

		parent::__construct();
	}

	protected function configure()
	{
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$books = [];

		for ($x = 1; $x <= 100; ++$x) {
			$books[$x] = $this->newBook($x);
		}

		$this->cache->set('api.books', $books);
	}

	private function newBook($id): Book
	{
		$faker = $this->faker;

		$book = new Book();
		$book->id = $id;
		$book->isbn = $faker->isbn13;
		$book->title = ucfirst($faker->words(mt_rand(2, 7), true));
		$book->author = $faker->name('male');
		$book->description = $faker->text;
		$book->publicationDate = $faker->dateTimeBetween('-30 years');

		return $book;
	}
}
