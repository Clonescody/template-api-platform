<?php

// api/src/Entity/Book.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * A book.
 *
 * @ApiResource()
 */
class Book
{
	/**
	 * @ApiProperty(identifier=true)
	 *
	 * @var int the id of this book
	 */
	public $id = 666;

	/**
	 * @var null|string the ISBN of this book (or null if doesn't have one)
	 */
	public $isbn = 'code_isbn';

	/**
	 * @var string the title of this book
	 */
	public $title = 'Title';

	/**
	 * @var string the description of this book
	 */
	public $description = 'Description';

	/**
	 * @var string the author of this book
	 */
	public $author = 'Author';

	/**
	 * @var \DateTimeInterface the publication date of this book
	 */
	public $publicationDate;

	public function __construct()
	{
		if (!isset($this->publicationDate)) {
			$this->publicationDate = new DateTime();
		}
	}

	public function getId(): ?int
	{
		return $this->id;
	}
}
