<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Book;
use Psr\SimpleCache\CacheInterface;

final class BookCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
	private $cache;

	public function __construct(CacheInterface $cache)
	{
		$this->cache = $cache;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return Book::class === $resourceClass;
	}

	public function getCollection(string $resourceClass, string $operationName = null): \Generator
	{
		$id = 0;

		for ($x = 0; $x < 100; ++$x) {
			yield $this->getBook(++$id);
		}
	}

	public function getBook($id): ?Book
	{
		$books = $this->cache->get('api.books');

		if (isset($books[$id])) {
			return $books[$id];
		}

		return null;
	}
}
