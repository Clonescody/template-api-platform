<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class FakeUserApi implements UserInterface
{
	public function getRoles(): array
	{
		return ['ROLE_API'];
	}

	public function getPassword()
	{
	}

	public function getSalt()
	{
	}

	public function getUsername()
	{
	}

	public function eraseCredentials()
	{
	}
}
