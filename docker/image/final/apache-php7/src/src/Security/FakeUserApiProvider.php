<?php

namespace App\Security;

use App\Entity\FakeUserApi;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class FakeUserApiProvider implements UserProviderInterface
{
	public function loadUserByUsername($username): UserInterface
	{
		return new FakeUserApi();
	}

	public function refreshUser(UserInterface $user): UserInterface
	{
		if (!$user instanceof FakeUserApi) {
			throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
		}

		return new FakeUserApi();
	}

	public function supportsClass($class): bool
	{
		return FakeUserApi::class === $class;
	}
}
