<?php

// api/src/DataProvider/BlogPostItemDataProvider.php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Book;
use Psr\SimpleCache\CacheInterface;

final class BookItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
	private $cache;

	public function __construct(CacheInterface $cache)
	{
		$this->cache = $cache;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return Book::class === $resourceClass;
	}

	public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Book
	{
		// Retrieve the blog post item from somewhere then return it or null if not found
		return $this->getBook($id);
	}

	public function getBook($id): ?Book
	{
		$books = $this->cache->get('api.books');

		if (isset($books[$id])) {
			return $books[$id];
		}

		return null;
	}
}
