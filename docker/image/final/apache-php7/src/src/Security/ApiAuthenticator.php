<?php

namespace App\Security;

use App\Entity\FakeUserApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
	private $secretApiKey;

	public function __construct(string $secretApiKey)
	{
		$this->secretApiKey = $secretApiKey;
	}

	public function supports(Request $request)
	{
		return $request->headers->has('SECRET-API-KEY');
	}

	public function getCredentials(Request $request)
	{
		return [
			'SECRET-API-KEY' => $request->headers->get('SECRET-API-KEY'),
		];
	}

	public function getUser($credentials, UserProviderInterface $userProvider): ?FakeUserApi
	{
		$apiToken = $credentials['SECRET-API-KEY'];

		if ($apiToken !== $this->secretApiKey) {
			return null;
		}

		return new FakeUserApi();
	}

	public function checkCredentials($credentials, UserInterface $user): bool
	{
		return true;
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?JsonResponse
	{
		return null;
	}

	public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?JsonResponse
	{
		$data = [
			'message' => 'Authentication Required',
		];

		return new JsonResponse($data, Response::HTTP_FORBIDDEN);
	}

	public function start(Request $request, AuthenticationException $authException = null): ?JsonResponse
	{
		$data = [
			'message' => 'Authentication Required',
		];

		return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
	}

	public function supportsRememberMe(): bool
	{
		return false;
	}
}
